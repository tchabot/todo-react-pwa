import React, {useState, useEffect} from 'react';
import './App.css';
import Login from './components/login/LogIn';
import Register from './components/register/Register';
import Dashboard from './components/task/TaskMain';
import {MuiThemeProvider, createMuiTheme} from '@material-ui/core/styles';
import {CssBaseline, CircularProgress} from '@material-ui/core';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import firebase from './components/firebase';

const theme = createMuiTheme();

export default function App() {

    const [firebaseInitialized, setFirebaseInitialized] = useState(false);

    useEffect(() => {
        firebase.isInitialized().then(val => {
            setFirebaseInitialized(val)
        })
    });

    return firebaseInitialized !== false ? (
        <MuiThemeProvider theme={theme}>
            <CssBaseline/>
            <Router>
                <Switch>
                    <Route exact path="/" component={Login}/>
                    <Route exact path="/login" component={Login}/>
                    <Route exact path="/register" component={Register}/>
                    <Route exact path="/dashboard" component={Dashboard}/>
                </Switch>
            </Router>
        </MuiThemeProvider>
    ) : <div id="loader"><CircularProgress/></div>
}
