import React from 'react';
import AppBar from '../menu/AppBar';
import firebase from '../firebase';
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import Checkbox from "@material-ui/core/Checkbox";
import Container from "@material-ui/core/Container";
import TaskModal from './TaskModal';
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from '@material-ui/icons/Delete';
import Button from "@material-ui/core/Button";

import '../../App.css';

class TaskMain extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            tasks: [],
            checked: [0]
        };
        this.deleteTask = this.deleteTask.bind(this);
        this.displayTasks = this.displayTasks.bind(this);
        this.handleToggle = this.handleToggle.bind(this);
        this.cleanDoneTasks = this.cleanDoneTasks.bind(this);
    }

    styles = {
        bottomButtonWrapper: {
            display: 'flex',
            justifyContent: 'center'
        },
        bottomButton: {
            position: 'fixed',
            bottom: 10
        },
        top20: {
            marginTop: '20px'
        },
        checkboxUrgent: {
            color: 'red'
        },
        checkboxHigh: {
            color: 'orange'
        },
        checkboxNormal: {
            color: 'green'
        },
        checkboxLow: {
            color: 'grey'
        }
    };

    componentDidMount() {
        this.displayTasks();
    }

    deleteTask(taskId) {
        let currentComponent = this;

        firebase.auth.currentUser.getIdToken(true)
            .then((idToken) => {
                    fetch('http://localhost:8080/tasks/' + taskId, {
                        method: 'DELETE',
                        headers: {
                            'Content-Type': 'application/json',
                            'ID_TOKEN': idToken,
                        },
                        body: JSON.stringify({
                            id: taskId,
                        })
                    }).then((response) => {
                        if (response.status >= 400) {
                            throw new Error("Bad response from server");
                        }
                        return response;
                    }).then(function () {
                        console.log('Tâche supprimée avec succès.');
                        currentComponent.displayTasks();
                    }).catch(function (err) {
                        console.log(err)
                    });
                }
            )
    }

    displayTasks() {
        let currentComponent = this;

        firebase.auth.currentUser.getIdToken(true)
            .then((idToken) => {
                fetch('http://localhost:8080/users/' + firebase.auth.currentUser.uid + '/tasks/', {
                    headers: {
                        'ID_TOKEN': idToken
                    }
                })
                    .then(res => res.json())
                    .then(
                        (result) => {
                            return currentComponent.setState({tasks: result});
                        },
                        (error) => {
                            console.log(error);
                        }
                    )
            })
    }

    cleanDoneTasks() {
        let currentComponent = this;

        firebase.auth.currentUser.getIdToken(true)
            .then((idToken) => {
                    fetch('http://localhost:8080/tasks/clean', {
                        method: 'DELETE',
                        headers: {
                            'Content-Type': 'application/json',
                            'ID_TOKEN': idToken,
                        }
                    }).then((response) => {
                        if (response.status >= 400) {
                            throw new Error("Bad response from server");
                        }
                        return response;
                    }).then(function () {
                        console.log('Tâches nettoyées avec succès.');
                        currentComponent.displayTasks();
                    }).catch(function (err) {
                        console.log(err)
                    });
                }
            )
    }

    handleToggle(value) {
        const {checked} = this.state;
        const currentIndex = checked.indexOf(value);
        const newChecked = [...checked];

        if (currentIndex === -1) {
            newChecked.push(value);
        } else {
            newChecked.splice(currentIndex, 1);
        }

        this.setState({
            checked: newChecked,
        });

        let currentComponent = this;

        firebase.auth.currentUser.getIdToken(true)
            .then((idToken) => {
                fetch('http://localhost:8080/tasks/' + value.id, {
                    method: 'PUT',
                    headers: {
                        'Content-Type': 'application/json',
                        'ID_TOKEN': idToken
                    },
                    body: JSON.stringify({
                        name: value.name,
                        done: value.done ? 0 : 1,
                        priority: value.priority
                    })
                }).then((response) => {
                    if (response.status >= 400) {
                        throw new Error("Bad response from server");
                    }
                    currentComponent.displayTasks();
                    return response.json();
                }).catch(function (err) {
                    console.log(err)
                });
            })
    }

    render() {
        if (!firebase.auth.currentUser) {
            this.props.history.replace('/login');
            return null
        }

        return (
            <div>
                <AppBar history={this.props.history}/>
                <Container>
                    <TaskModal displayTasks={this.displayTasks}/>
                </Container>
                <div style={this.styles.top20}>
                    {this.state.tasks.map(task => (
                        <ListItem key={task.id} role={undefined} dense button
                                  onClick={() => this.handleToggle(task)}>
                            <Checkbox
                                checked={task.done}
                                tabIndex={-1}
                                disableRipple
                                style={task.priority.id === 1 ? this.styles.checkboxUrgent
                                    : task.priority.id === 2 ? this.styles.checkboxHigh
                                    : task.priority.id === 3 ? this.styles.checkboxNormal
                                    : this.styles.checkboxLow}
                            />
                            <ListItemText key={task.id} primary={task.name}
                                          className={task.done ? 'StripedTask' : ''}/>
                            <IconButton aria-label="Comments" onClick={() => this.deleteTask(task.id)}>
                                <DeleteIcon/>
                            </IconButton>
                        </ListItem>
                    ))}
                </div>
                <div style={this.styles.bottomButtonWrapper}>
                    <Button
                        style={this.styles.bottomButton}
                        variant="outlined"
                        color="secondary"
                        onClick={this.cleanDoneTasks}
                    >
                        Nettoyer tâches terminées
                    </Button>
                </div>
            </div>
        )
    }
}

export default TaskMain;
