import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import FormHelperText from "@material-ui/core/FormHelperText";
import firebase from '../firebase';
import {DatePicker, MuiPickersUtilsProvider} from "@material-ui/pickers";
import frLocale from "date-fns/locale/fr";
import LocalizedUtils from './CustomDatePicker';

class FormDialog extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            open: false,
            tasks: [],
            checked: [0],
            taskName: '',
            priority: '',
            priorities: [],
            hasError: false,
            targetDate: new Date()
        };
        this.handleTaskNameChange = this.handleTaskNameChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleClickOpen = this.handleClickOpen.bind(this);
        this.handleClose = this.handleClose.bind(this);
        this.handlePriorityChange = this.handlePriorityChange.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleDateChange = this.handleDateChange.bind(this);
    }

    styles = {
        addTaskButton: {
            display: 'flex',
            justifyContent: 'center',
            marginTop: '20px'
        },
        top20: {
            marginTop: '20px'
        }
    };

    componentDidMount() {
        let currentComponent = this;

        firebase.auth.currentUser.getIdToken(true)
            .then(function (idToken) {
                fetch('http://localhost:8080/priorities/', {
                    headers: {
                        'ID_TOKEN': idToken
                    }
                })
                    .then(res => res.json())
                    .then((result) => {
                        currentComponent.setState({
                            priorities: result
                        });
                    })
                    .catch((error) => {
                        console.log(error);
                    })
            });
    }

    handleClickOpen() {
        this.setState({
            open: true
        });
    }

    handleClose() {
        this.setState({
            open: false
        })
    }

    handleTaskNameChange(event) {
        this.setState({taskName: event.target.value});
    }

    handleChange(event) {
        this.setState({
            [event.target.name]: event.target.value
        });
    }

    handlePriorityChange(event) {
        this.setState({
            priority: event.target.value,
            hasError: false
        })
    }

    handleDateChange(date) {
        this.setState({
            targetDate: date
        });
    }

    formatDate(date) {
        let d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;

        return [year, month, day].join('-');
    }

    // Create a task
    handleSubmit(event) {
        event.preventDefault();

        if (!this.state.priority) {
            this.setState({
                hasError: true
            });
        } else {
            let currentComponent = this;
            let dateToSave = this.formatDate(this.state.targetDate);

            firebase.auth.currentUser.getIdToken(true)
                .then((idToken) => {
                    fetch('http://localhost:8080/tasks', {
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/json',
                            'ID_TOKEN': idToken
                        },
                        body: JSON.stringify({
                            name: currentComponent.state.taskName,
                            priority: currentComponent.state.priority,
                            done: false,
                            userId: firebase.auth.currentUser.uid,
                            targetDate: dateToSave
                        })
                    }).then((response) => {
                        if (response.status >= 400) {
                            throw new Error("Bad response from server");
                        }
                        currentComponent.setState({
                            taskName: '',
                            priority: ''
                        });
                        currentComponent.props.displayTasks();
                        return response;
                    }).then(function () {
                        console.log('Tâche correctement enregistrée');
                    }).catch(function (err) {
                        console.log(err)
                    });

                    currentComponent.setState({open: false});
                });
        }
    }

    render() {
        return (
            <div style={this.styles.addTaskButton}>
                <Button variant="contained" color="primary" onClick={this.handleClickOpen}>
                    Ajouter une tâche
                </Button>
                <Dialog open={this.state.open} onClose={this.handleClose} aria-labelledby="form-dialog-title">
                    <DialogTitle id="form-dialog-title">Ajouter une tâche</DialogTitle>
                    <DialogContent>
                        <form onSubmit={this.handleSubmit}>
                            <FormControl margin="normal" required fullWidth>
                                <TextField
                                    id="taskName"
                                    name="taskName"
                                    label="Intitulé"
                                    value={this.state.taskName}
                                    onChange={this.handleChange}
                                    variant="outlined"
                                    required
                                />
                            </FormControl>
                            <FormControl margin="normal" required fullWidth>
                                <InputLabel shrink htmlFor="priority">Priorité</InputLabel>
                                <Select
                                    id="priority"
                                    label="Priorité"
                                    value={this.state.priority}
                                    onChange={this.handlePriorityChange}
                                    required
                                >
                                    {this.state.priorities.map((priority) => {
                                        return <MenuItem key={priority.id} value={priority}>{priority.name}</MenuItem>
                                    })}
                                </Select>
                                {this.state.hasError && <FormHelperText>Sélectionnez une priorité</FormHelperText>}
                            </FormControl>
                            <MuiPickersUtilsProvider utils={LocalizedUtils} locale={frLocale}>
                                <DatePicker
                                    clearable
                                    format="d MMM yyyy"
                                    value={this.state.handleDateChange}
                                    onChange={(value) => this.handleDateChange(value)}
                                    clearLabel="vider"
                                    cancelLabel="annuler"
                                    name="targetDate"
                                />
                            </MuiPickersUtilsProvider>
                            <Button
                                fullWidth
                                variant="contained"
                                color="primary"
                                type="submit"
                                style={this.styles.top20}
                            >
                                Ajouter
                            </Button>
                        </form>
                    </DialogContent>
                    <DialogActions>
                        <Button
                            onClick={this.handleClose}
                            fullWidth
                            variant="contained"
                            color="default"
                        >
                            Retour
                        </Button>
                    </DialogActions>
                </Dialog>
            </div>
        );
    }
}

export default FormDialog;
