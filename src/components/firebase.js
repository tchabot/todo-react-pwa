import app from 'firebase/app'
import 'firebase/auth'

const config = {
    apiKey: "AIzaSyAHbo2cMPfJoRy548XxBsogUG2JFeWAAIc",
    authDomain: "todo-react-pwa-88c27.firebaseapp.com",
    databaseURL: "https://todo-react-pwa-88c27.firebaseio.com",
    projectId: "todo-react-pwa-88c27",
    storageBucket: "todo-react-pwa-88c27.appspot.com",
    messagingSenderId: "480341196388",
};

class Firebase {

    constructor() {
        app.initializeApp(config);
        this.auth = app.auth();
    }

    login(email, password) {
        return this.auth.signInWithEmailAndPassword(email, password)
    }

    logout() {
        return this.auth.signOut()
    }

    async register(email, password) {
        await this.auth.createUserWithEmailAndPassword(email, password);
    }

    isInitialized() {
        return new Promise(resolve => {
            this.auth.onAuthStateChanged(resolve)
        });
    }

    getCurrentUsername() {
        return this.auth.currentUser && this.auth.currentUser.displayName;
    }

}

export default new Firebase()
